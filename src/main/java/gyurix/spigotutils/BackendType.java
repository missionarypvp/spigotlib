package gyurix.spigotutils;

/**
 * Created by GyuriX on 2016.01.06..
 */
public enum BackendType {
    FILE, MYSQL
}
